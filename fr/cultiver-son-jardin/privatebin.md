# Installation de PrivateBin

[PrivateBin][1] est un logiciel libre qui permet de partager des textes de
manière confidentielle et sécurisée.
Il s’agit d’un fork de Zerobin développé initialement par [Sébastien Sauvage][2].

![](images/privatebin/privatebin.png)

Les données sont chiffrées dans le navigateur web en utilisant
[l’algorithme <attr title="Advanced Encryption Standard">AES</attr> 256 bits][3],
le serveur reçoit les données et l’identifiant du document mais ne
connaît pas la clé qui permet de déchiffrer le texte.
Nous proposons ce logiciel comme service en ligne sur [Framabin][4].
Il est extrêmement simple à installer sur un serveur, peu gourmand en
ressources et ne requiert aucune base de données.

![](images/privatebin/framabin.jpg)

Voici un tutoriel pour vous aider à l’installer sur votre serveur.

<p class="alert alert-info">
  <span class="label label-primary">Informations</span> Dans la suite de
  ce tutoriel, nous supposerons que vous avez déjà fait pointer votre
  nom de domaine sur votre serveur auprès de votre
  <a href="https://fr.wikipedia.org/wiki/Bureau_d%27enregistrement">registraire</a>
   et que vous disposez d'un serveur web et de PHP en version supérieure à 5.4.
</p>

## Installation

### 1 - Planter

![](images/icons/semer-pot.png)

Téléchargez les fichiers de la dernière version sur le [dépôt Github officiel][5].
Décompressez l'archive et uploadez le tout sur votre serveur web.<br>
Pour utiliser le logiciel il suffit de vous rendre sur
`http://votre-site.org/PrivateBin/`
(vous pouvez évidemment renommer le dossier).<br>
Le dossier `PrivateBin/data` doit être
accessible en écriture par l'utilisateur.<br>
Copiez ensuite le fichier `cfg/conf.sample.php` dans `cfg/conf.php` et
[modifiez si besoin les paramètres](https://github.com/PrivateBin/PrivateBin/wiki/Configuration).

### 2 - Tailler et désherber

![](images/icons/tailler.png)

Pour personnaliser l’interface vous pouvez choisir un autre theme en
remplaçant le paramètre `template = "bootstrap"` par
celui de votre choix parmi ceux qui existent dans le dossier `tpl`.

Vous pouvez évidemment modifier directement les fichiers `tpl/*.php`.

<i class="glyphicon glyphicon-tree-conifer" aria-hidden="true"></i>

 [1]: https://privatebin.info
 [2]: http://sebsauvage.net
 [3]: https://fr.wikipedia.org/wiki/Advanced_Encryption_Standard
 [4]: https://framabin.org
 [5]: https://github.com/PrivateBin/PrivateBin/releases/latest
 [6]: https://framagit.org/framasoft/framabin/tree/zerobin-fr/
 [7]: https://framagit.org/framasoft/framabin/tree/master
