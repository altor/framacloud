# Cultiver son jardin

<div class="alert alert-success" style="margin-top:25px;">
    <p>Les tutoriels de la rubrique «&nbsp;<strong>Cultiver son jardin</strong>&nbsp;»
    concernent les logiciels que <b class="frama">Frama</b><b class="soft">soft</b>
    propose comme services en ligne libres dans le cadre de la campagne de
    «&nbsp;<a href="http://degooglisons-internet.org">dégooglisation</a>&nbsp;».</p>
    <p>Ils sont destinés à un public qui souhaiterait <strong>aller plus
    loin dans l’émancipation</strong>.</p>
    <p>Si vous n’avez jamais hébergé votre propre site web, ces tutoriels
    ne seront peut-être pas pour vous.<br>Certains sont simples,
    d’autres beaucoup moins mais c’est peut-être une excellente occasion d’apprendre.</p>
    <p>À travers ces tutoriels, nous voulons encourager un maximum de
    personnes <strong>à installer</strong> –&nbsp;ou <strong>demander de
    faire installer</strong>, ce qui est tout aussi important –&nbsp;chez elles,
    au sein de leur entreprise, association, établissement scolaire… ces
    logiciels pour contribuer à la <strong>décentralisation du net</strong>.</p>
</div>

<!--
  La liste est classés par ordre alphabetique de Framaservice
-->
<div class="clearfix">
  <div class="col-md-3 col-sm-6">
    <a href="nextcloud.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-calendar" aria-hidden="true"></i></p>
      <p><b class="violet">Fram</b><b class="vert">agenda</b></p>
      <p><b>Nextcloud</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="wallabag.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-briefcase" aria-hidden="true"></i></p>
      <p><b class="violet">Frama</b><b class="vert">bag</b></p>
      <p><b>Wallabag</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="searx.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-search" aria-hidden="true"></i></p>
      <p><b class="violet">Frama</b><b class="vert">bee</b></p>
      <p><b>Searx</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="privatebin.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-paste" aria-hidden="true"></i></p>
      <p><b class="violet">Frama</b><b class="vert">bin</b></p>
      <p><b>PrivateBin</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="kanboard.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-dashboard" aria-hidden="true"></i></p>
      <p><b class="violet">Frama</b><b class="vert">board</b></p>
      <p><b>Kanboard</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="bicbucstriim.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-coffee" aria-hidden="true"></i></p>
      <p><b class="violet">Frama</b><b class="vert">bookin</b></p>
      <p><b>Bicbucstriim</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="umap.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-map" aria-hidden="true"></i></p>
      <p><b class="violet">Frama</b><b class="vert">carte</b></p>
      <p><b>uMap</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="dolomon.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-hand-pointer-o" aria-hidden="true"></i></p>
      <p><b class="violet">Frama</b><b class="vert">clic</b></p>
      <p><b>Dolomon</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="framadate.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-calendar-check-o" aria-hidden="true"></i></p>
      <p><b class="violet">Frama</b><b class="vert">date</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="nextcloud.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-cloud-upload" aria-hidden="true"></i></p>
      <p><b class="violet">Frama</b><b class="vert">drive</b></p>
      <p><b>Nextcloud</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="lufi.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-send" aria-hidden="true"></i></p>
      <p><b class="violet">Frama</b><b class="vert">drop</b></p>
      <p><b>Lufi</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="framaforms.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-list-ul" aria-hidden="true"></i></p>
      <p><b class="violet">Frama</b><b class="vert">forms</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="gitlab.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-git" aria-hidden="true"></i></p>
      <p><b class="violet">Frama</b><b class="vert">git</b></p>
      <p><b>Gitlab</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="lstu.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-link" aria-hidden="true"></i></p>
      <p><b class="violet">Frama</b><b class="vert">link</b></p>
      <p><b>Lstu</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="sympa.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-group" aria-hidden="true"></i></p>
      <p><b class="violet">Frama</b><b class="vert">listes</b></p>
      <p><b>Sympa</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="framaestro.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-magic" aria-hidden="true"></i></p>
      <p><b class="violet">Fra</b><b class="vert">maestro</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="scrumblr.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-object-group" aria-hidden="true"></i></p>
      <p><b class="violet">Fra</b><b class="vert">memo</b></p>
      <p><b>Scrumblr</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="wisemapping.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-sitemap" aria-hidden="true"></i></p>
      <p><b class="violet">Fra</b><b class="vert">mindmap</b></p>
      <p><b>Wisemapping</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="minetest.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-cube" aria-hidden="true"></i></p>
      <p><b class="violet">Fra</b><b class="vert">minetest</b></p>
      <p><b>Minetest</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="turtl.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-newspaper-o" aria-hidden="true"></i></p>
      <p><b class="violet">Frama</b><b class="vert">news</b></p>
      <p><b>TinyTinyRSS</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="turtl.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-sticky-note" aria-hidden="true"></i></p>
      <p><b class="violet">Frama</b><b class="vert">notes</b></p>
      <p><b>Trutl</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="mastodon.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-mastodon" aria-hidden="true"></i></p>
      <p><b class="violet">Frama</b><b class="vert">piaf</b></p>
      <p><b>Mastodon</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="lutim.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-photo" aria-hidden="true"></i></p>
      <p><b class="violet">Frama</b><b class="vert">pic</b></p>
      <p><b>Lutim</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="etherpad.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-align-left" aria-hidden="true"></i></p>
      <p><b class="violet">Frama</b><b class="vert">pad</b></p>
      <p><b>Etherpad</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="grav.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-globe" aria-hidden="true"></i></p>
      <p><b class="violet">Frama</b><b class="vert">site</b></p>
      <p><b>Grav</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="framaslides.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-pie-chart" aria-hidden="true"></i></p>
      <p><b class="violet">Frama</b><b class="vert">slides</b></p>
      <p><b>Strut</b></p>
    </a>
  </div>
<!--
  <div class="col-md-3 col-sm-6">
    <a href="diaspora.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-diaspora" aria-hidden="true"></i></p>
      <p><b class="violet">Frama</b><b class="vert">sphère</b></p>
      <p><b>Diaspora*</b></p>
    </a>
  </div>
-->
  <div class="col-md-3 col-sm-6">
    <a href="jitsi-meet.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-video-camera" aria-hidden="true"></i></p>
      <p><b class="violet">Frama</b><b class="vert">talk</b></p>
      <p><b>Jitsi Meet</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="gitlab.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-comments-o" aria-hidden="true"></i></p>
      <p><b class="violet">Frama</b><b class="vert">team</b></p>
      <b>Mattermost</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="svg-edit.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-paint-brush" aria-hidden="true"></i></p>
      <p><b class="violet">Frama</b><b class="vert">vectoriel</b></p>
      <p><b>SVG-Edit</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="loomio.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-bullhorn" aria-hidden="true"></i></p>
      <p><b class="violet">Frama</b><b class="vert">vox</b></p>
      <p><b>Loomio</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="shaarli.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-star" aria-hidden="true"></i></p>
      <p><b class="violet">My</b><b class="vert">Frama</b></p>
      <p><b>Shaarli</b></p>
    </a>
  </div>
</div>
