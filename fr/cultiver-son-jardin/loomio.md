# Installation de Loomio

![](images/loomio/logo.png)

[Loomio][1] est le logiciel de prise de décision collective que nous
proposons comme service en ligne sur [Framavox][2].

![](images/loomio/loomio.png)

Voici un tutoriel pour vous aider à l’installer sur votre serveur.
Il s’appuie sur [la procédure officielle][3].

<p class="alert alert-info">
    <span class="label label-primary">Informations</span><br /> Dans la
    suite de ce tutoriel, les instructions seront données pour un serveur
    dédié sous Ubuntu 14.04 en utilisant Docker.
</p>

## Installation

### 1 - Préparer la terre

![](images/icons/preparer.png)

#### DNS

Tout d’abord, faites pointer `votre-domaine.org` sur votre serveur
auprès de votre [registraire][4] ainsi que le sous-domaine `faye.votre-domaine.org`
(il s’agit du service qui tourne en arrière-plan pour envoyer
les mails et notifications aux utilisateurs).

Voici par exemple ce que nous avons mis pour framavox.org dans notre zone DNS :

    @ 3600 IN A 46.4.207.243
    faye 3600 IN CNAME framavox.org.
    @ 10800 IN MX 2 framavox.org.

#### Docker

Ensuite, connectez-vous en tant que `root` sur votre serveur, installez
Docker et Docker Compose.

    wget -q https://get.docker.com/ -O install-docker.sh
    # Vérifiez le contenu du script pour vous assurer qu'il ne fait pas de bêtises sur votre système
    sh install-docker.sh
    wget -O /usr/local/bin/docker-compose https://github.com/docker/compose/releases/download/1.6.2/docker-compose-`uname -s`-`uname -m`
    chmod +x /usr/local/bin/docker-compose

### 2 - Semer

![](images/loomio/semer.png)

Depuis le dossier `/root`, téléchargez les fichiers du dépôt officiel pour un déploiement *en production*.

    cd /root
    git clone https://github.com/loomio/loomio-deploy.git
    cd loomio-deploy

<p class="alert alert-info">
  Toutes les instructions suivantes s’effectueront depuis le dossier
  <code>/root/loomio-deploy</code>.
</p>

#### Swap (facultatif)

Si votre serveur dispose de moins de 4GB de RAM, il est recommandé de
créer un fichier de swap.

    ./scripts/create_swapfile

#### Configuration

Créez ensuite les fichiers de configuration de Loomio.

    ./scripts/create_env votre-domaine.org vous@adresse.email

Le script crée 2 fichiers `env` et `faye_env` qui seront utilisés pour configurer les différents containers Docker :

*   `nginx`, le serveur web
*   `letsencrypt`, pour créer automatiquement les certificats SSL et disposer d’un site en https
*   `loomio`, la partie visible par les navigateurs web
*   `worker`, `faye` et `mailin` qui traitent en arrière-plan les requêtes pour l’envoi des courriels, des notifications et autres interactions
*   `db`, le serveur de base de données (PostgreSQL)

##### Courriels

Modifiez le fichier `env` pour configurer l’envoi des courriels avec un serveur SMTP.

    SMTP_DOMAIN=votre-domaine.org
    SMTP_SERVER=serveur.mail.org
    SMTP_PORT=587
    SMTP_USERNAME=utilisateur@mail.org
    SMTP_PASSWORD=votre-mot-de-passe

Les messages seront envoyés avec l’adresse `notification@votre-domaine.org`.
Pour permettre la bonne transmission des mails lorsque les utilisateurs
répondent à un courriel de notification, nous avons dû adapter la
configuration du container `mailin` dans `docker-compose.yml`.

    mailin:
      image: loomio/mailin-docker
      ports:
        - "25:25"
      links:
        - loomio
      environment:
        - WEBHOOK_URL=https://votre-domaine.org/email_processor/

**NB** Si vous avez déjà un logiciel de mail sur votre serveur,
il n’aimera pas être dépossédé du port 25. Vous pouvez le couper
(`service exim4 stop`) et le désactiver (`update-rc.d exim4 disable`)
ou le faire écouter sur un autre port (`/etc/default/exim4`).

##### Tâches planifiées

Pour prévenir les utilisateurs de la fermeture imminente d’une proposition,
la clôturer ou encore envoyer un courriel récapitulatif de l’activité de la veille,
Loomio doit exécuter certains processus périodiquement.

Pour cela, copiez le fichier `crontab` dans `/etc/cron.d`

    mv ./crontab /etc/cron.d/loomio

##### Service

Pour disposer d’une commande d’arrêt/relance de Loomio ou pour qu’il
reste actif après un redémarrage du serveur, vous pouvez créer
[un fichier `/etc/init.d/loomio`][5] (à rendre exécutable) contenant ceci :

    #! /bin/sh

    ### BEGIN INIT INFO
    # Provides:             loomio
    # Required-Start:       $remote_fs $syslog
    # Required-Stop:        $remote_fs $syslog
    # Default-Start:        2 3 4 5
    # Default-Stop:
    # Short-Description:    Loomio dockers services
    ### END INIT INFO

    set -e

    umask 022

    . /lib/lsb/init-functions

    case "$1" in
      start)
            cd /root/loomio-deploy/
            /usr/local/bin/docker-compose start
            log_end_msg 0 || true
            ;;
      stop)
            cd /root/loomio-deploy/
            /usr/local/bin/docker-compose stop
            log_end_msg 0 || true
            ;;
      logs)
            cd /root/loomio-deploy/
            /usr/local/bin/docker-compose logs
            log_end_msg 0 || true
            ;;
      restart)
            cd /root/loomio-deploy/
            /usr/local/bin/docker-compose restart
            ;;

      *)
            log_action_msg "Usage: /etc/init.d/ssh {start|stop|restart|logs}" || true
            exit 1
    esac

    exit 0

### 3 - Arroser

![](images/loomio/arroser.png)

#### PostgreSQL

Il faut maintenant créer la base de données.

    docker-compose run loomio rake db:setup

#### Loomio

La base de données est prête, vous pouvez démarrer Loomio.

    docker-compose up -d

La commande se charge de lancer tous les containers nécessaires.
Pour pouvez vérifier leur statut en lançant la commande `docker-compose logs`
ou `service loomio logs`

#### Tests

Voici une liste de tâches à effectuer pour s’assurer que tout fonctionne bien :

*   Créez un compte et un groupe puis déconnectez-vous.
*   Demandez une réinitialisation de mot de passe et vérifiez que vous
    recevez bien le courriel.
*   Ouvrez une page de discussion dans 2 onglets et saisissez un
    commentaire dans l’un. Le commentaire devrait apparaître dynamiquement
    sur l’autre onglet.
*   Uploadez un fichier à une discussion et un avatar personnalisé à
    votre compte ou à un groupe.
*   Vérifiez que vous pouvez répondre à une discussion par courriel.
*   Créée une proposition avec une date de clôture proche pour vérifier
    qu’elle se ferme automatiquement dans l’heure.

### 4 - Pailler

![](images/icons/pailler.png)

#### Personnalisation

À ce stade, si tout s’est bien passé, lorsque vous consultez le
site `votre-domaine.org`, vous devriez être redirigé vers la page de connexion.

Si vous voulez utiliser une page d’accueil personnalisée, il suffit de
télécharger [le plugin loomio.org][8] (le code du site officiel de Loomio) dans le dossier `/root/loomio-deploy`
et d’ajouter le dossier à la liste des « volumes » dans `docker-compose.yml` :

      - ./loomio_org_plugin:/loomio/plugins/loomio_org_plugin

La redirection se fera ainsi sur `votre-domaine.org/marketing`
et vous pourrez en personnaliser le contenu.

![](images/loomio/loomio_org.jpg)

Vous pouvez ensuite vous inspirer de [ce que nous avons fait pour Framavox][6].
Nous avons créé un dossier `framavox/template` dans lequel se trouvent
les fichiers à remplacer dans le container et nous avons modifié le
fichier `docker-compose.yml` en fonction.

    volumes:
      - ./framavox/template/angular.html.haml:/loomio/app/views/layouts/angular.html.haml
      - ./framavox/template/application.html.haml:/loomio/app/views/layouts/application.html.haml
      - ./framavox/template/marketing.html.haml:/loomio/plugins/loomio_org_plugin/views/pages/marketing.html.haml
      - ./framavox/template/_metadata.html.haml:/loomio/app/views/angular/_metadata.html.haml
      - ./framavox/template/_social_metadata.html.haml:/loomio/app/views/application/_social_metadata.html.haml

Même chose avec les fichiers de traduction qui étaient incomplets.

      - ./framavox/locales/client.fr.yml:/loomio/config/locales/client.fr.yml
      - ./framavox/locales/fr.yml:/loomio/config/locales/fr.yml
      - ./framavox/locales/server.fr.yml:/loomio/config/locales/server.fr.yml

L’essentiel de l’interface de Loomio se trouve [dans `/loomio/app/views/`][7].
Le reste se trouve [dans les fichiers de langue][9].

Avant d’effectuer des modifications au fichier `docker-compose.yml` ou
au fichier `env`, pensez à supprimer proprement les containers
`docker-compose down` et les redémarrer ensuite `docker-compose up -d`

#### Quelques commandes utiles

*   `docker exec -it loomiodeploy_loomio_1 /bin/bash` pour entrer dans
    le container `loomio` en cours d’utilisation (pour voir précisément
    où se trouvent les fichiers à modifier par exemple)
*   `docker restart loomiodeploy_container_1` redémarre un container
    (pour vérifier les changements dans un fichier de template par
    exemple sans devoir tout redémarrer)
*   `docker ps -a` affiche la liste de tous les containers présent sur la machine
*   `docker stop loomiodeploy_container_1` arrête un container
*   `docker rm loomiodeploy_container_1` le supprime
*   `docker-compose pull` pour mettre à jour tous les containers
    (après un `docker-compose down`)

<i class="glyphicon glyphicon-tree-deciduous" aria-hidden="true"></i>

 [1]: https://www.loomio.org/
 [2]: https://framavox.org
 [3]: https://github.com/loomio/loomio-deploy/
 [4]: http://fr.wikipedia.org/wiki/Bureau_d%27enregistrement
 [5]: https://framagit.org/framasoft/framavox/blob/master/service
 [6]: https://framagit.org/framasoft/framavox/blob/master/docker-compose.yml
 [7]: https://github.com/loomio/loomio/tree/master/app/views/
 [8]: https://github.com/loomio/loomio_org_plugin/tree/master/views/pages
 [9]: https://github.com/loomio/loomio/tree/master/config/locales